from tkinter import Label, Button, Entry, Frame, Checkbutton
from tkinter import TOP, BOTTOM, RIGHT, LEFT, X, IntVar, DoubleVar, BooleanVar, StringVar
from tkinter.ttk import Combobox
from tkinter import Toplevel, messagebox, filedialog

import traceback, ast


def create_tkvars(pen_param, descr, config):
    tkvars = {key: None for key in config.keys()}
    for key in config.keys():
        if config[key]['type'] == 'int':
            tkvars[key] = IntVar(value=pen_param[key])
        elif config[key]['type'] == 'float':
            tkvars[key] = DoubleVar(value=pen_param[key])
        elif config[key]['type'] == 'string' or config[key]['type'] == 'list':
            tkvars[key] = StringVar(value=pen_param[key])
        elif config[key]['type'] == 'bool':
            tkvars[key] = BooleanVar(value=pen_param[key])

    # Name is not a laser param but may be modified
    tkvars.update({'descr': StringVar(value=str(descr))})
    return tkvars
def build_popup(main_window, number, descr, config, tkvars):
    # Init Window
    window = Toplevel(main_window)
    window.focus()
    window.grab_set()
    window.resizable(False, False)
    window.title("Pen configuration")

    # Titre Frame
    title_frame = Frame(window)
    title_frame.pack(side=TOP, pady=(15, 15))
    titre = Label(title_frame, text="Pen "+str(number))
    titre.configure(font=("", 16, "bold"))
    titre.pack(side=TOP)
    sub_frame = Frame(title_frame)
    sub_frame.pack(side=BOTTOM)
    Label(sub_frame, text="Description").pack(side=LEFT)
    Entry(sub_frame, textvariable=tkvars['descr'], width=30).pack(side=RIGHT)

    # Custom Frame
    grid_frame = Frame(window)
    grid_frame.pack()
    name_width = 15
    value_width = 10
    unit_width = 10

    for key in config.keys():
        gridrow = config[key]["row"]
        gridcol = int(config[key]["col"] * 3)

        # Names
        Label(grid_frame, text=config[key]["name"], anchor='w', width=name_width).grid(row=gridrow,column=gridcol)

        # Entries
        if config[key]['type'] in ['int', 'float', 'string']:
            Entry(grid_frame, textvariable=tkvars[key], width=value_width).grid(row=gridrow, column=gridcol + 1)
        elif config[key]['type'] == "bool":
            Checkbutton(grid_frame, variable=tkvars[key], width=value_width).grid(row=gridrow, column=gridcol + 1)
        elif config[key]['type'] == 'list':
            list_widget = Combobox(grid_frame, textvariable=tkvars[key], width=7)
            list_widget['values'] = config[key]['values']
            list_widget['state'] = 'readonly'
            list_widget.grid(row=gridrow, column=gridcol + 1)

        # Units
        Label(grid_frame, text=config[key]["unit"], anchor='w', width=unit_width).grid(row=gridrow,column=gridcol + 2)

    return window
def check_tkvars_validity(config, tkvars):
    for key in tkvars.keys():
        try:
            tmp = tkvars[key].get()
        except:
            titre = "Wrong value"
            mess = traceback.format_exc()
            return False, (titre, mess)

    return True, ""
def format_descr_to_filename(string):

    # Remove Windows Unwanted characters
    unwanted_char = ['/', '\\', '*', "?", '"', ">", "<", "|", ":"]
    for uc in unwanted_char:
        string = string.replace(uc, "_")

    return string


class Pen():
    def __init__(self, config, number, description=None, src_pen=None, action='add'):
        """
        Initiate a Pen with multiple context parameters.
        :param config: mandatory : the configuration options from the app. this constructor needs to know from which Laser the Pen has to be constructed from
        :param number: unique identifier of the Pen, this cannot be changed by the user
        :param description: (optionnal) write the description of the Pen
        :param src_pen: (optionnal for 'add' action but mandatory for the other actions)
        :param action: can take 3 values:
            add : We want to create a pen with default parameters. Only the number is not default
            add+ : We want to create a new Pen but the values are taken from the src_pen
            modify : We just want to modify the actual Pen. Therefore we copy every parameters

        """
        self.__validated = False

        # Init Values without src_pen
        if action == 'add':
            self.__number = number
            self.__descr = "Pen " + str(number) if description == None else "Pen " + str(number) + " : " + str(description)
            self.__params = {}
            for key in config['pen_param'].keys():
                self.__params.update({key: config['pen_param'][key]['default']})

            self.__config = config['pen_param']

        elif action == 'add+': # Init Values with a src_pen (function add+)
            if src_pen != None:
                self.__number = number
                self.__descr = "Pen " + str(number) + " (from Pen " + str(src_pen.get('number')) + ")"
                if description != None:
                    self.__descr += str(description)

                self.__params = {}
                for key in config['pen_param'].keys():
                    self.__params.update({key: src_pen.get('params')[key]})
                self.__config = config['pen_param']
            else:
                print("Error : No src_pen given for 'add+' command")

        elif action == 'modify':
            if src_pen != None:
                self.__number = src_pen.get('number')
                self.__descr = src_pen.get('descr')
                self.__params = src_pen.get('params')
                self.__config = config['pen_param']

    # Concerning pen popup
    def __callback_validate(self, window):
        ret, err = check_tkvars_validity(self.__config, self.__tkvars)

        if ret:
            self.__validated = True
            for key in self.__params.keys():
                self.__params[key] = self.__tkvars[key].get()
            self.__descr = self.__tkvars['descr'].get()
            window.destroy()
            del self.__tkvars

        else:
            messagebox.showerror(err[0], err[1])
    def __callback_cancel(self, window):
        self.__validated = False
        window.destroy()
        del self.__tkvars
    def display_popup(self, main_window):
        # Methods
        self.__tkvars = create_tkvars(self.__params, self.__descr, self.__config)
        window = build_popup(main_window, self.__number, self.__descr, self.__config, self.__tkvars)

        # Default button frame
        po_button_frame = Frame(window)
        po_button_frame.pack(side=BOTTOM, fill=X, pady=(10, 10), padx=(10, 10))
        Button(po_button_frame, text="Validate", command=lambda: self.__callback_validate(window), width=15).pack(side=RIGHT)
        Button(po_button_frame, text="Cancel", command=lambda: self.__callback_cancel(window) , width=15).pack(side=RIGHT)

        main_window.wait_window(window)

    # I/O
    def import_from_file(self, default_path=None):
        if default_path == None:
            f = filedialog.askopenfile(title="Import Pen",
                                       mode='r',
                                       defaultextension=".pen",
                                       filetypes=[('PEN', '.pen')])
            if f is None:
                return False
        else:
            f = filedialog.askopenfile(title="Import Pen",
                                       mode='r',
                                       defaultextension=".pen",
                                       filetypes=[('PEN', '.pen')],
                                       initialdir=default_path)
            if f is None:
                return False
        content = f.read()
        filename = f.name.split("/")[-1].replace(".pen","")
        f.close()

        try:
            # Read the content as dict()
            params = ast.literal_eval(content)

            # Reinitialise the __params
            self.__params = {}
            for key in params.keys():
                self.__params.update({key: params[key]})

            # Set the description as the name of the file
            self.__descr = "Pen " + str(self.__number) + " (imported from " + str(filename) + ")"
            return True

        except:
            messagebox.showerror(title="Corrupted PEN file",
                                 message="Error : " + str(traceback.format_exc()))
            return False
    def export_to_file(self, default_path=None):
        if default_path == None:
            f = filedialog.asksaveasfile(title="Export PEN",
                                         mode='w',
                                         defaultextension=".pen",
                                         initialfile=format_descr_to_filename(self.__descr),
                                         filetypes=[('PEN', '.pen')])
            if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
                return False
        else:
            f = filedialog.asksaveasfile(title="Export PEN",
                                         mode='w',
                                         defaultextension=".pen",
                                         initialfile=format_descr_to_filename(self.__descr),
                                         filetypes=[('PEN', '.pen')],
                                         initialdir=default_path)
            if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
                return False

        params_to_string = str(self.__params).replace(',', ',\r').replace('{', '{\r ').replace('}', '\r}')
        f.write(params_to_string)
        f.close()

        return True

    # Accessors
    def print(self):
        print("\n\nNumber : ", self.__number)
        print("Description : ", self.__descr)
        for key in self.__params.keys():
            print(key, "\t\t\t", self.__params[key])
        print("\n")
    def get(self, attribute=None):
        if attribute == None:
            return {'number': self.__number,
                    'descr': self.__descr,
                    'params': self.__params,
                    'config': self.__config}
        elif attribute == "params":
            return self.__params
        elif attribute == "number":
            return self.__number
        elif attribute == "descr":
            return self.__descr
        elif attribute == 'valid':
            return self.__validated


