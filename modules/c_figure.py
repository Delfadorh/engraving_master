from tkinter import Label, Button, Entry, Frame, Checkbutton, Canvas
from tkinter import TOP, BOTTOM, RIGHT, LEFT, X, IntVar, DoubleVar, BooleanVar, StringVar, CENTER
from tkinter.ttk import Combobox
from tkinter import Toplevel, messagebox, filedialog
from PIL import Image, ImageTk

import traceback, ast

from modules.img_utils import img_import, resize_keep_aspect



def build_fig_type_vector_gene_frame(vector_gene_frame, tkvars, full_config):
    # LEFT SIDE
    left_side_frame = Frame(vector_gene_frame)
    left_side_frame.pack(side=LEFT)
    Label(left_side_frame, text="Vectors Generator").pack(side=TOP)
    # Vector frame image
    vector_logo = img_import(full_config['paths']['vector_img'])
    vector_logo = resize_keep_aspect(vector_logo, 150,150)
    photo = ImageTk.PhotoImage(vector_logo)
    image_label = Label(left_side_frame, image=photo, anchor=CENTER)
    image_label.image = photo
    image_label.pack(side=BOTTOM)

    # SEPARATOR
    Canvas(vector_gene_frame, bg='black', height=200, width=1).pack(side=LEFT)

    # PARAMETER FRAME
    right_side_frame = Frame(vector_gene_frame)
    right_side_frame.pack(side=RIGHT)
    name_width = 17
    value_width= 10
    unit_width = 10

    config = full_config['figure_param']
    Label(right_side_frame, text="Général", font=("-weight bold")).grid(row=0, column=1)

    for key in config.keys():
        if config[key]['param_group'] == "general":
            continue
        elif config[key]['param_group'] == "vector_gene":
            gridrow = config[key]["row"]
            gridcol = 1
        else:
            continue

        # Names
        Label(right_side_frame, text=config[key]["name"], anchor='w', width=name_width).grid(row=gridrow,column=gridcol)

        # Entries
        if config[key]['type'] in ['int', 'float', 'string']:
            Entry(right_side_frame, textvariable=tkvars[key], width=value_width).grid(row=gridrow, column=gridcol + 1)
        elif config[key]['type'] == "bool":
            Checkbutton(right_side_frame, variable=tkvars[key], width=value_width).grid(row=gridrow, column=gridcol + 1)
        elif config[key]['type'] == 'list':
            list_widget = Combobox(right_side_frame, textvariable=tkvars[key], width=7)
            list_widget['values'] = config[key]['values']
            list_widget['state'] = 'readonly'
            list_widget.grid(row=gridrow, column=gridcol + 1)

        # Units
        Label(right_side_frame, text=config[key]["unit"], anchor='w', width=unit_width).grid(row=gridrow,column=gridcol + 2)

    return vector_gene_frame
def create_tkvars(figure_param, descr, fig_type, full_config):
    config = full_config['figure_param'] # Filter keys
    tkvars = {key: None for key in config.keys()}
    for key in config.keys():
        if config[key]['type'] == 'int':
            tkvars[key] = IntVar(value=figure_param[key])
        elif config[key]['type'] == 'float':
            tkvars[key] = DoubleVar(value=figure_param[key])
        elif config[key]['type'] == 'string' or config[key]['type'] == 'list':
            tkvars[key] = StringVar(value=figure_param[key])
        elif config[key]['type'] == 'bool':
            tkvars[key] = BooleanVar(value=figure_param[key])

    # Name is not a laser param but may be modified
    tkvars.update({'descr': StringVar(value=str(descr)),
                   'fig_type': StringVar(value=str(fig_type))})
    return tkvars
def check_tkvars_validity(config, tkvars):
    for key in tkvars.keys():
        try:
            tmp = tkvars[key].get()
        except:
            titre = "Wrong value"
            mess = traceback.format_exc()
            return False, (titre, mess)

    return True, ""
def format_descr_to_filename(string):

    # Remove Windows Unwanted characters
    unwanted_char = ['/', '\\', '*', "?", '"', ">", "<", "|", ":"]
    for uc in unwanted_char:
        string = string.replace(uc, "_")

    return string


class Figure():
    def __init__(self, config, number, description=None, src_figure=None, action='add'):
        self.__validated = False

        # Init Values without src_pen
        if action == 'add':
            self.__number = number
            self.__descr = "Figure " + str(number) if description == None else "Figure " + str(number) + " : " + str(
                description)
            self.__fig_type = config['fig_type']['default']
            self.__params = {}
            for key in config['figure_param'].keys():
                self.__params.update({key: config['figure_param'][key]['default']})
            self.__full_config = config


        elif action == 'add+':  # Init Values with a src_figure (function add+)
            if src_figure != None:
                self.__number = number
                self.__descr = "Figure " + str(number) + " (from Figure " + str(src_figure.get('number')) + ")"
                if description != None:
                    self.__descr += str(description)
                self.__fig_type = src_figure.get('fig_type')
                self.__params = {}
                for key in config['figure_param'].keys():
                    self.__params.update({key: src_figure.get('params')[key]})
                self.__full_config = config
            else:
                print("Error : No src_figure given for 'add+' command")

        elif action == 'modify':
            if src_figure != None:
                self.__number = src_figure.get('number')
                self.__descr = src_figure.get('descr')
                self.__fig_type = src_figure.get('fig_type')
                self.__fig_type_values = config['fig_type']['values']
                self.__params = src_figure.get('params')
                self.__full_config = config

    def __fig_type_switch_frame(self,e):
        if self.__figure_type_widget.get() == "Image":
            self.__fig_type_vector_gene_frame.pack_forget()
            self.__fig_type_image_frame.pack(side=BOTTOM)
        elif self.__figure_type_widget.get() == "Vector Generator":
            self.__fig_type_image_frame.pack_forget()
            self.__fig_type_vector_gene_frame.pack(side=BOTTOM)

    def __callback_validate(self, window):
            ret, err = check_tkvars_validity(self.__full_config, self.__tkvars)

            if ret:
                self.__validated = True
                for key in self.__params.keys():
                    self.__params[key] = self.__tkvars[key].get()
                self.__descr = self.__tkvars['descr'].get()
                self.__fig_type = self.__tkvars['fig_type'].get()
                window.destroy()
                del self.__tkvars

            else:
                messagebox.showerror(err[0], err[1])

    def __callback_cancel(self, window):
        self.__validated = False
        window.destroy()
        del self.__tkvars

    def __callback_browse_image(self):
        f = filedialog.askopenfile(title="Import Image",
                                   mode='r',
                                   defaultextension=".png",
                                   filetypes=[('PNG', '.png')])
        if f is None:
            return

        self.__tkvars['image_filepath'].set(f.name)
        figure_logo = img_import(f.name, True)
        self.__tkvars['image_dimension'].set(str(figure_logo.size))
        self.__tkvars['image_filename'].set(f.name.split('/')[-1])

        # Vector frame image
        figure_logo = resize_keep_aspect(figure_logo, 150, 150)
        photo = ImageTk.PhotoImage(figure_logo)

        image_label = Label(self.left_side_frame, image=photo, anchor=CENTER)
        image_label.image = photo
        image_label.pack(side=BOTTOM)

    def __build_fig_type_image_frame(self, image_frame, tkvars, full_config):

        self.left_side_frame = Frame(image_frame, width=200)
        self.left_side_frame.pack(side=LEFT, fill='both', expand=True)

        Label(self.left_side_frame, text="Image", width=10).pack(side=TOP, padx=(50, 50))
        Button(self.left_side_frame, text='Import', width=15, command=self.__callback_browse_image).pack()

        img_info_frame = Frame(self.left_side_frame, width=200)
        Label(img_info_frame, text="filename : ", anchor='w').grid(row=0)
        Label(img_info_frame, textvariable=tkvars["image_filename"]).grid(row=0, column=1)
        Label(img_info_frame, text="dimensions : ", anchor='w').grid(row=1)
        Label(img_info_frame, textvariable=tkvars["image_dimension"]).grid(row=1, column=1)
        img_info_frame.pack(fill='both', expand=True)

        # SEPARATOR
        Canvas(image_frame, width=1, height=200, bg='black').pack(side=LEFT)

        # RIGHT SIDE FRAME
        right_side_frame = Frame(image_frame)
        right_side_frame.pack(side=RIGHT)

        name_width = 17
        value_width = 10
        unit_width = 10
        config = full_config['figure_param']
        Label(right_side_frame, text="Général", font=("-weight bold")).grid(row=0, column=1)
        for key in config.keys():
            if config[key]['param_group'] == "general":
                continue
            elif config[key]['param_group'] == "image":
                gridrow = config[key]["row"]
                gridcol = 1
            else:
                continue

            # Names
            Label(right_side_frame, text=config[key]["name"], anchor='w', width=name_width).grid(row=gridrow,
                                                                                                 column=gridcol)

            # Entries
            if config[key]['type'] in ['int', 'float', 'string']:
                Entry(right_side_frame, textvariable=tkvars[key], width=value_width).grid(row=gridrow,
                                                                                          column=gridcol + 1)
            elif config[key]['type'] == "bool":
                Checkbutton(right_side_frame, variable=tkvars[key], width=value_width).grid(row=gridrow,
                                                                                            column=gridcol + 1)
            elif config[key]['type'] == 'list':
                list_widget = Combobox(right_side_frame, textvariable=tkvars[key], width=7)
                list_widget['values'] = config[key]['values']
                list_widget['state'] = 'readonly'
                list_widget.grid(row=gridrow, column=gridcol + 1)

            # Units
            Label(right_side_frame, text=config[key]["unit"], anchor='w', width=unit_width).grid(row=gridrow,
                                                                                                 column=gridcol + 2)

    def __build_main_window(self, main_window):
        # Init Window
        window = Toplevel(main_window)
        window.focus()
        window.grab_set()
        window.resizable(False, False)
        window.title("Pen configuration")

        # Titre Frame
        title_frame = Frame(window)
        title_frame.pack(side=TOP, pady=(15, 15))
        titre = Label(title_frame, text="Figure " + str(self.__number))
        titre.configure(font=("", 16, "bold"))
        titre.pack(side=TOP)
        sub_frame = Frame(title_frame)
        sub_frame.pack()
        Label(sub_frame, text="Description").pack(side=LEFT)
        Entry(sub_frame, textvariable=self.__tkvars['descr'], width=30).pack(side=RIGHT)
        sub_frame2 = Frame(title_frame)
        sub_frame2.pack(side=BOTTOM)
        Label(sub_frame2, text="Figure Type").pack(side=LEFT)

        self.__figure_type_widget = Combobox(sub_frame2, textvariable=self.__tkvars['fig_type'], width=27)
        self.__figure_type_widget['values'] = self.__full_config['fig_type']['values']
        self.__figure_type_widget['state'] = 'readonly'
        self.__figure_type_widget.pack(side=RIGHT)

        # Image Frame
        self.__fig_type_image_frame = Frame(window, width=500, height=500)
        tk_img_info = [StringVar(), StringVar()]
        self.__build_fig_type_image_frame(self.__fig_type_image_frame, self.__tkvars, self.__full_config)

        # Vector Frame
        self.__fig_type_vector_gene_frame = Frame(window, width=500, height=500)
        self.__fig_type_vector_gene_frame = build_fig_type_vector_gene_frame(self.__fig_type_vector_gene_frame,
                                                                             self.__tkvars, self.__full_config)

        self.__figure_type_widget.bind('<<ComboboxSelected>>', self.__fig_type_switch_frame)
        self.__figure_type_widget.event_generate("<<ComboboxSelected>>")



        # Default button frame
        po_button_frame = Frame(window)
        po_button_frame.pack(side=BOTTOM, fill=X, pady=(10, 10), padx=(10, 10))
        Button(po_button_frame, text="Validate", command=lambda: self.__callback_validate(window), width=15).pack(
            side=RIGHT)
        Button(po_button_frame, text="Cancel", command=lambda: self.__callback_cancel(window), width=15).pack(
            side=RIGHT)

        main_window.wait_window(window)

    def display_popup(self, main_window):
        # Methods
        self.__tkvars = create_tkvars(self.__params, self.__descr, self.__fig_type, self.__full_config)
        self.__build_main_window(main_window)





    def get(self, attribute):
        if attribute == None:
            return {'number': self.__number,
                    'descr': self.__descr,
                    'params': self.__params,
                    'config': self.__full_config}
        elif attribute == "params":
            return self.__params
        elif attribute == "number":
            return self.__number
        elif attribute == "descr":
            return self.__descr
        elif attribute == "fig_type":
            return self.__fig_type
        elif attribute == 'valid':
            return self.__validated

    def print(self):
        print("\n\nNumber : ", self.__number)
        print("Description : ", self.__descr)
        for key in self.__params.keys():
            print(key, "\t\t\t", self.__params[key])
        print("\n")



class ImageFigure(Figure):
    def __init__(self, config, number, description=None, src_figure=None, action='add'):
        super().__init__(config, number, description=None, src_figure=None, action='add')

    def display_popup(self, main_window):
        # Init Window
        window = Toplevel(main_window)
        window.focus()
        window.grab_set()
        window.resizable(False, False)
        window.title("Figure Configuration")

        # Titre Frame
        title_frame = Frame(window)
        title_frame.pack(side=TOP, pady=(15, 15))
        titre = Label(title_frame, text="Figure " + str(self.__number))
        titre.configure(font=("", 16, "bold"))
        titre.pack(side=TOP)
        sub_frame = Frame(title_frame)
        sub_frame.pack()
        Label(sub_frame, text="Description").pack(side=LEFT)
        Entry(sub_frame, textvariable=self.__tkvars['descr'], width=30).pack(side=RIGHT)
        sub_frame2 = Frame(title_frame)
        sub_frame2.pack(side=BOTTOM)
        Label(sub_frame2, text="Figure Type").pack(side=LEFT)

