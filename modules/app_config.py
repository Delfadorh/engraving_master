import os, ast, traceback
from tkinter import Tk, messagebox, Button, Listbox, Label, StringVar, END



class app_config():
    def __init__(self, default_laser=False):
        # public variables
        self.CONFIG = {}

        # private variables
        self.ROOT = os.getcwd().replace("\\", '/')
        self.app_config_file = "app_config.txt"

        # Methods
        self.__read_config_file()
        if default_laser == False:
            self.window = Tk()
            self.window.eval('tk::PlaceWindow . center')
            self.__build_window()
            self.window.mainloop()
        else:
            self.CONFIG['paths']['selected_laser_cwd'] = self.CONFIG['paths']["default_laser_folder"]

    def __read_config_file(self):
        # Read App config general
        try :
            f = open(self.app_config_file, 'r')  # Lecture dans le cwd du programme
            content = f.read()
            f.close()
            self.CONFIG = ast.literal_eval(content)
            print("Read App Config file correctly")

        except:
            traceback.print_exc()
            title = "Corrupted file"
            message = "Unable to read the App Configuration file.\n ERROR :" + traceback.format_exc()
            messagebox.showerror(title, message)
            return False

        # Prepend general CWD to the app_config_paths
        for p in self.CONFIG['paths'].keys():
            self.CONFIG['paths'][p] = self.ROOT + self.CONFIG['paths'][p]

        # Read default objects config : FIGURE
        try:
            f = open(self.CONFIG['paths']['figure_config_path'], 'r')
            content = f.read()
            f.close()
            self.CONFIG.update(ast.literal_eval(content))
            print("Read App Config file correctly")
        except:
            traceback.print_exc()
            title = "Corrupted file"
            message = "Unable to read the Figure default configuration file.\n ERROR :" + traceback.format_exc()
            messagebox.showerror(title, message)
            return False

        return True

    def __build_window(self):
        laser_folder = self.CONFIG['paths']['lasers_folder']
        laser_list = os.listdir(laser_folder)

        Label(self.window, text="Select a Laser").pack()
        self.listb = Listbox(self.window, height=8, width=30)
        self.listb.pack()
        self.listb.bind('<Double-Button-1>', self.__callback_validate)
        Button(self.window, text="Validate", command=self.__callback_validate).pack()

        for las in laser_list:
            self.listb.insert(END, las)

    def __callback_validate(self,e=None):
        if len(self.listb.curselection()) == 1:
            selected = self.listb.get(self.listb.curselection())
            tmp_filepath = self.CONFIG['paths']['lasers_folder'] + "/" + str(selected)

            # assign CONFIG
            self.CONFIG['paths']['selected_laser_cwd'] = tmp_filepath

            if self.__read_laser_config(tmp_filepath):
                self.window.destroy()

    def __read_laser_config(self, filepath):
        f = open(self.CONFIG['paths']['selected_laser_cwd'] + "/laser_config.txt", 'r')
        content = f.read()
        f.close()

        try :
            LASER_CONF = ast.literal_eval(content)
            self.CONFIG.update(LASER_CONF)
            print("Read laser_config.txt correctly")
            return True
        except:
            traceback.print_exc()
            title = "Corrupted file"
            message = "Laser Configuration file is corrupted.\nERROR : " + traceback.format_exc()
            messagebox.showerror(title, message)
            return False

    def get_app_config(self):
        return self.CONFIG


