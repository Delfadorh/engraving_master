
from modules.app_config import app_config
from modules.c_figure import Figure
from modules.c_pen import Pen
import tkinter as tk

penlist = []
figlist = []

def format_descr_to_filename(string):

    # Remove Windows Unwanted characters
    unwanted_char = ['/', '\\', '*', "?", '"', ">", "<", "|", ":"]
    print("filter")
    for uc in unwanted_char:
        string = string.replace(uc, "_")

    return string
def get_first_available_number(penlist):
    nums = [pen.get('number') for pen in penlist]
    free_number = 1
    while free_number in nums:
        free_number += 1
    return free_number


def add_pen():
    p = Pen(c, get_first_available_number(penlist), action='add')
    p.display_popup(win)
    if p.get('valid'):
        penlist.append(p)

def add_pen_from(pen_obj):
    p = Pen(c, get_first_available_number(penlist), src_pen=pen_obj, action='add+')
    p.display_popup(win)
    if p.get('valid'):
        penlist.append(p)

def modify_pen(pen_obj):
    p = Pen(c, 100, src_pen=pen_obj, action='modify')
    p.display_popup(win)
    if p.get('valid'):
        penlist[0] = p

def print_penlist():
    print(penlist)
    for pen in penlist:
        pen.print()



c = app_config()
print(c.get_app_config()['paths']['selected_laser_cwd'])
c = c.get_app_config()



win = tk.Tk()
tk.Button(win, text='add pen', command=add_pen).pack()
tk.Button(win, text='add pen from', command=lambda: add_pen_from(penlist[0])).pack()
tk.Button(win, text='modify pen', command=lambda: modify_pen(penlist[0])).pack()
tk.Button(win, text='print pen', command=print_penlist).pack()

f = Figure(c, get_first_available_number(figlist), action='add')
f.display_popup(win)
if f.get('valid'):
    figlist.append(f)
    print(f.print())


win.mainloop()