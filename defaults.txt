{
	### Default paths ###
	"laser_config_filepath" : 	"/Lasers/CO2_Taufenbach/laser_config.txt",
	"defaults_opt" : 			"/defaults/options/defaults_options.txt",
	"vector_img" : 				"/defaults/images/vector.png",
	"saving_pen_path" : 		"/saved/pens",
	"default_export_path" : 	"/img_input",
	"path_to_co2_exe" : 		"/Lasers/CO2_Taufenbach/GcodeConverter",
	
	### Lasers ###
	"figure_param":{
		'fig_number': {"name": "Figure Number", "type": "int", "min": 1, "max": 255, "default": 1, "unit": "", "allow_mtx_gene": False},
		"fig_custom_name": {"name": "Name", "type": "string", "default": " ", "allow_mtx_gene": False},
		"size_X": {"name" : "Size X", "type": "float", "min": 0.1, "max": 60, "default": 30, "unit": "mm", "allow_mtx_gene": False},
		"size_Y": {"name" : "Size Y", "type": "float", "min": 0.1, "max": 60, "default": 30, "unit": "mm", "allow_mtx_gene": False},
		"position_X": {"name" : "Position X", "type": "float", "min": -30, "max": 30, "default": 0, "unit": "mm", "allow_mtx_gene": False},
		"position_Y": {"name" : "Position Y", "type": "float", "min": -30, "max": 30, "default": 0, "unit": "mm", "allow_mtx_gene": False},
		"fig_rotation": {"name" : "Rotation", "type": "float", "min": -360, "max": 360, "default": 0, "unit": "deg", "allow_mtx_gene": True},
		"fig_type" : {"name": "Type", "type": 'list', "values": ["image", "vecteur"], "default":"vecteur", "allow_mtx_gene": False},
		"v_position_X": {"name" : "Position X", "type": "float", "min": -30, "max": 30, "default": 0, "unit": "mm", "allow_mtx_gene": False},
		"v_position_Y": {"name" : "Position Y", "type": "float", "min": -30, "max": 30, "default": 0, "unit": "mm", "allow_mtx_gene": False},
		"v_length" : {"name": "Length", "type": "float", "min": 0.01, "max": 84.0, "default": 10, "unit": "mm", "allow_mtx_gene": True},
		"v_angle" : {"name": "Direction", "type": "float", "min": -360.0, "max": 360.0, "default": 0, "unit": "deg", "allow_mtx_gene": True},
		"v_repeat_angle" : {"name": "Direction", "type": "float", "min": -360.0, "max": 360.0, "default": 90, "unit": "deg", "allow_mtx_gene": False},
		"v_repeat_length" : {"name": "Distance", "type": "float", "min": 0.01, "max": 84.0, "default": 3, "unit": "mm", "allow_mtx_gene": False},
		"v_repeat_number" : {'name': "Nombre de vecteurs", "type":'int', "min": 1, "max": 1000000, "default": 1, "unit": "", "allow_mtx_gene": True},
		"canvas_resolution" : {'name': "app hatch", "type":'float', "min": 459, "max": 461, "default": 460, "unit": "", "allow_mtx_gene": False}
	},
	"matrix_gene_default":{
		"size_X": {"name" : "Size X", "type": "float", "min": 0.1, "max": 60, "default": 30, "unit": "mm", "is_default_setting": True},
		"size_Y": {"name" : "Size Y", "type": "float", "min": 0.1, "max": 60, "default": 30, "unit": "mm", "is_default_setting": True},
		"spacing_X": {"name" : "Spacing X", "type": "float", "min": 0.1, "max": 60, "default": 1, "unit": "mm", "is_default_setting": True},
		"spacing_Y": {"name" : "Spacing Y", "type": "float", "min": 0.1, "max": 60, "default": 1, "unit": "mm", "is_default_setting": True},
		"offset_X": {"name" : "Offset X", "type": "float", "min": -30, "max": 30, "default": 0, "unit": "mm", "is_default_setting": True},
		"offset_Y": {"name" : "Offset Y", "type": "float", "min": -30, "max": 30, "default": 0, "unit": "mm", "is_default_setting": True},
		"selected_horiz_param": {"name": "Horizontal", "type": "string", "default": " ", "is_default_setting": False},
		"selected_horiz_param_min": {"name": "Horiz Param Min", "type": "float", "default": 0, "unit": "", "is_default_setting": False},
		"selected_horiz_param_max": {"name": "Horiz Param Max","type": "float", "default": 0, "unit": "", "is_default_setting": False},
		"nb_value_horiz_param": {"name": "Horiz Param Nb","type": "int", "min": 0, "max": 255, "default": 0, "unit": "", "is_default_setting": False},
		"selected_verti_param": {"name": "Vertical", "type": "string", "default": " ", "is_default_setting": False},
		"selected_verti_param_min": {"name": "Verti Param Min","type": "float", "default": 0, "unit": "", "is_default_setting": False},
		"selected_verti_param_max": {"name": "Verti Param Max","type": "float", "default": 0, "unit": "", "is_default_setting": False},
		"nb_value_verti_param": {"name": "Horiz Param Nb","type": "int", "min": 0, "max": 255, "default": 0, "unit": "", "is_default_setting": False},
		"fig_selection": {"name": "Select Figure", "type": "string", "default": " ", "is_default_setting": False}
	},
	"job_param":{
		"job_number": {"name": "Job Number", "type": "int", "min": 1, "max": 1000, "default": 1, "unit": ""},
		'job_custom_name': {"name": "Name", "type": "string", "default": " "},
		'temp_order': {'name':"tmp", 'type': "int", "default": 1},
		"temp_figure": {'name':"tmp", 'type': "string", "default": " "},
		"temp_pen": {'name':"tmp", 'type': "string", "default": " "},
		"export_path": {'name':"tmp", 'type': "string", "default": "/img_input"}
	}
}