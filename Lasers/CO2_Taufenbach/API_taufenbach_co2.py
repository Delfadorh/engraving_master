import os, time, io, ast
from ftplib import FTP
from telnetlib import Telnet
import subprocess
import traceback

ROOT = os.getcwd().replace('\\', '/')
laser_config_filepath = ROOT + "/Lasers/CO2_Taufenbach/laser_config.txt"
path_to_co2_exe = os.getcwd().replace("\\", "/") + "/Lasers/CO2_Taufenbach/GcodeConverter"

def read_config():
    if os.path.exists(laser_config_filepath):
        f = open(laser_config_filepath, 'r')
        content = f.read()
        f.close()
        conf = ast.literal_eval(content)
        return conf
    else:
        print(laser_config_filepath, "does not exist")

# Funuctions concerning CNC generation
def append_beginning():
    debut = "G17\nG71\nG90\n$SC_DEFINES = 1\nSC_SELECT_JOB[6]\n\n"
    return debut
def import_galvo_param(galvo):
    galvo_string = str()
    galvo_string += "(Begin Galvo parameters)\n"
    galvo_string += "$SC_FIELDXMIN = " + str(galvo["galvo_X_min"]['default']) + "\n"
    galvo_string += "$SC_FIELDXMAX = " + str(galvo["galvo_X_max"]['default']) + "\n"
    galvo_string += "$SC_FIELDYMIN = " + str(galvo["galvo_Y_min"]['default']) + "\n"
    galvo_string += "$SC_FIELDYMAX = " + str(galvo["galvo_Y_max"]['default']) + "\n"
    galvo_string += "$SC_FIELDXGAIN = " + str(galvo["galvo_X_gain"]['default']) + "\n"
    galvo_string += "$SC_FIELDYGAIN = " + str(galvo["galvo_Y_gain"]['default']) + "\n"
    galvo_string += "$SC_FIELDXOFFSET = 0\n"
    galvo_string += "$SC_FIELDYOFFSET = 0\n"
    galvo_string += "$SC_FIELDAXISSTATE = " + str(galvo["galvo_field_axis_state"]['default']) + "\n"
    galvo_string += "(End Galvo parameters)\n\n"
    return galvo_string
def import_pen(pen):
    pen_string = str()
    pen_string += "(Begin Pen "+str(pen.dic['pen_number']["value"])+" : "+str(pen.dic['pen_custom_name']["value"])+")\n"
    pen_string += "SC_TOOL_LOAD[" + str(pen.dic['pen_number']["value"]) + "]\n"
    pen_string += "$SC_LASERPOWER = " + str(pen.dic['laser_power']["value"]) + "\n"
    pen_string += "$SC_FREQUENCY = " + str(pen.dic['frequency']["value"]) + "\n"
    pen_string += "$SC_STDBYPERIOD = " + str(pen.dic['stdby_period']["value"]) + "\n"
    pen_string += "$SC_JUMPSPEED = " + str(pen.dic['jumpspeed']["value"]) + "\n"
    pen_string += "$SC_MARKSPEED = " + str(pen.dic['mark_speed']["value"]) + "\n"
    pen_string += "$SC_JUMPDELAY = " + str(pen.dic['jump_delay']["value"]) + "\n"
    pen_string += "$SC_MARKDELAY = " + str(pen.dic['mark_delay']["value"]) + "\n"
    pen_string += "$SC_POLYDELAY = " + str(pen.dic['poly_delay']["value"]) + "\n"
    pen_string += "$SC_LASERONDELAY = " + str(pen.dic['laserON_delay']["value"]) + "\n"
    pen_string += "$SC_LASEROFFDELAY = " + str(pen.dic['laserOFF_delay']["value"]) + "\n"
    pen_string += "SC_TOOL_SAVE[" + str(pen.dic['pen_number']["value"]) + "]\n"
    pen_string += "(End Pen " + str(pen.dic['pen_number']["value"]) + " : " + str(pen.dic['pen_custom_name']["value"]) + ")\n\n"
    return pen_string
def begin_main():
    string = str()
    string += "(Begin Main)\n"
    string += "$SC_ArcStepMode = 1\n"
    string += "$SC_DEFINES = 0\n"
    return string
def get_fig_name(fig):
    custom_name_controled = fig.dic['fig_custom_name']['value'].replace(".", "_").replace(" ", "_")
    return "Figure" + str(fig.dic['fig_number']["value"]) + "_" + custom_name_controled + "\n"
def add_main_element(fig,pen):
    nb_passage = str(pen.dic['nb_passage']['value'])
    pen_number = str(pen.dic['pen_number']['value'])
    main_string = str()
    main_string += "$SC_LoopCount = " + nb_passage + "\n"
    main_string += "SC_Start_Entity\n"
    main_string += "SC_TOOL_LOAD["+pen_number+"]\n"
    main_string += "CALL " + get_fig_name(fig) + "\n"
    main_string += "SC_End_Entity\n\n"
    return main_string
def end_main():
    end_string = str()
    end_string += "SC_TOOL[0]\n"
    end_string += "G0 X0 Y0\n"
    end_string += "M2\n\n\n"
    return end_string
def import_figure(figure):

    figure_string = str()
    figure_string += "DFS "+ get_fig_name(figure) + "\n"
    for vec in figure.vectors:
        if vec[2] == 1:
            figure_string += "SC_TOOL[0]\n"
            figure_string += "G0 X" + str(vec[0][0]) + " Y" + str(vec[0][1]) + "\n"
            figure_string += "SC_TOOL[1]\n"
            figure_string += "G1 X" + str(vec[1][0]) + " Y" + str(vec[1][1]) + "\n"

    figure_string += "ENDDFS\n\n"
    return figure_string
def import_warmup(time_in_s): #
    string = str()
    string += "(Begin WARMUP)\n"
    string += "SC_TOOL_LOAD[255]\n"
    string += "$SC_LASERPOWER = 5\n"
    string += "$SC_FREQUENCY = 44\n"
    string += "$SC_STDBYPERIOD = 0\n"
    string += "$SC_JUMPSPEED = 9000\n"
    string += "$SC_MARKSPEED = 2000\n"
    string += "$SC_JUMPDELAY = 0\n"
    string += "$SC_MARKDELAY = 0\n"
    string += "$SC_POLYDELAY = 0\n"
    string += "$SC_LASERONDELAY = 70\n"
    string += "$SC_LASEROFFDELAY = 100\n"
    string += "SC_TOOL_SAVE[255]\n"
    string += "SC_TOOL_LOAD[255]\n"
    string += "SC_TOOL[0]\n"
    string += "G0 X-25 Y25\n"
    string += "SC_TOOL[1]\n"
    string += "$SC_LoopCount = " + str(round(10*time_in_s)) + "\n"
    string += "SC_Start_Entity\n"
    string += "G1 X-25 Y25\n"
    string += "G1 X25 Y25\n"
    string += "G1 X25 Y-25\n"
    string += "G1 X-25 Y-25\n"
    string += "SC_End_Entity\n"
    string += "(End Warmup)\n\n"

    return string

class CNC():
    def __init__(self, job):
        galvo = read_config()['galvo_default']

        self.CNC = str()
        self.CNC += append_beginning()
        self.CNC += import_galvo_param(galvo)

        # Append every pen
        for ord in job.sequence.keys():
            for fig in job.sequence[ord].keys():
                for pen in job.sequence[ord][fig].keys():
                    pen_obj = job.sequence[ord][fig][pen]['pen_object']
                    self.CNC += import_pen(pen_obj)

        # Append Main
        self.CNC += begin_main()

        for ord in job.sequence.keys():
            for fig in job.sequence[ord].keys():
                for pen in job.sequence[ord][fig].keys():
                    pen_obj = job.sequence[ord][fig][pen]['pen_object']
                    fig_obj = job.sequence[ord][fig][pen]['figure_object']
                    if pen_obj.dic["need_warmup"]['value']:
                        self.CNC += import_warmup(10)
                    self.CNC += add_main_element(fig_obj, pen_obj)

        # End main
        self.CNC += end_main()

        # Start function calling at the end
        for ord in job.sequence.keys():
            for fig in job.sequence[ord].keys():
                temp_pen = 0
                for pen in job.sequence[ord][fig].keys():
                    temp_pen = pen
                fig_obj = job.sequence[ord][fig][temp_pen]['figure_object']

                self.CNC += import_figure(fig_obj)

    def save_as_file(self, filepath):
        try:
            buffer = open(filepath, "w")
            buffer.write(self.CNC)
            buffer.close()
            print("CNC length", len(self.CNC.split('\n')))
            return True, ""
        except:
            return False, traceback.format_exc()

class Laser():
    def __init__(self):
        conf = read_config()
        self.laser_ip = conf["communication"]["laser_ip"]["value"]
        self.laser_telnet_timeout = conf["communication"]["telnet_timeout"]["value"]
        self.interlock = conf["communication"]["laser_interlock"]["value"]

        self.ftp = None
        self.laser_is_on = False
        self.laser_is_ready = False

    def connect(self):

        # Connection to Telnet
        try:
            telnet = Telnet(self.laser_ip, timeout=self.laser_telnet_timeout)
            telnet.read_until(b"INET> ", self.laser_telnet_timeout)
            telnet.write(b"M \r\n")
            # We need to wait a bit for the response from the laser.
            time.sleep(0.5)
            laser_status = telnet.read_eager()
            # If laser_status contains 0:1, the laser is on
            # laser_on = "0:1" in str(laser_status)
            # If laser_status contains 0:0, the laser is off
            laser_off = "0:0" not in str(laser_status)
            if laser_off:
                self.laser_is_on = False
            else:
                self.laser_is_on = True
            telnet.close()

        except Exception as e:
            print(e)
            return False

        # Connection to FTP
        try:
            self.ftp = FTP(self.laser_ip, timeout=self.laser_telnet_timeout)
            self.ftp.login()
        except Exception as e:
            self.ftp = None
            print(e)
            return False

        return True

    def check_status(self):
        telnet = Telnet(self.laser_ip, timeout=self.laser_telnet_timeout)
        telnet.read_until(b"INET> ", self.laser_telnet_timeout)
        telnet.read_until(b"INET> ", self.laser_telnet_timeout)
        telnet.write(b"OIU\r\n")
        time.sleep(0.5)
        status = telnet.read_eager()
        laser_ready = self.interlock not in str(status)
        self.laser_is_ready = laser_ready
        telnet.close()
        return  laser_ready

    def ping(self):
        command = ['ping', '-n', '1', self.laser_ip]
        t0 = time.time()
        out = subprocess.call(command, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        t1 = time.time()
        time.sleep(1)
        if out == 0:
            return True, round(1000*(t1-t0))
        else:
            return False, -1

    def send(self, filepath): # filepath is CNC file
        print("filepath to send", filepath)
        if not os.path.exists(filepath):
            print("No file found at ", filepath)
            return False

        # Convert to UNF
        prev_path = os.getcwd()
        file_name = filepath.split('/')[-1]
        os.chdir(path_to_co2_exe)
        out = os.system("gconv_static.exe -co2 " + filepath + " >nul 2>&1")
        os.chdir(prev_path)
        #File creates in the filapth with name filename.unf

        # Connect to laser
        if self.connect():
            self.ftp.cwd("jobs")
            f = io.open(filepath.replace('.cnc', '.unf'), "rb")
            self.ftp.storbinary('STOR ' + file_name.replace('.cnc', '.unf'), f)  # upload du fichier
            f.close()
            self.ftp.close()

    def print(self, filepath, galvo_rotation):
        file = filepath.split("/")[-1].replace('.cnc','').encode()
        galvo_rotation = str(galvo_rotation).encode()
        if self.connect():
            if self.check_status():
                try:
                    telnet = Telnet(self.laser_ip, timeout=self.laser_telnet_timeout)
                    telnet.read_until(b"INET> ", self.laser_telnet_timeout)
                    telnet.write(b"FM 0\r\n")  # Ne sert pas specialement mais empeche les bugs
                    time.sleep(1)
                    telnet.write(b"FM 0\r\n")  # Idem, pour etre sur d avoir 0:
                    telnet.read_until(b'0:\r\n')
                    telnet.write(b"JLA 6 " + file + b"\r\n")  # positionnement du fichier sur le job 6 du server
                    telnet.read_until(b"0:\r\n")
                    telnet.write(b"JN 6\r\n")  # selection du job 6 pour la gravure
                    telnet.read_until(b"0:\r\n")

                    telnet.write(b"RT " + galvo_rotation + b"\r\n")  # rotation correction
                    telnet.read_until(b"0:\r\n")
                    telnet.write(b"M 1\r\n")  # gravure
                    telnet.close()

                except Exception as e:
                    print(e)

