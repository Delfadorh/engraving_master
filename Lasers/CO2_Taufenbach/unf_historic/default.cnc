G17
G71
G90
$SC_DEFINES = 1
SC_SELECT_JOB[6]

(Begin Galvo parameters)
$SC_FIELDXMIN = -30
$SC_FIELDXMAX = 30
$SC_FIELDYMIN = -30
$SC_FIELDYMAX = 30
$SC_FIELDXGAIN = 1
$SC_FIELDYGAIN = 1
$SC_FIELDXOFFSET = 0
$SC_FIELDYOFFSET = 0
$SC_FIELDAXISSTATE = 3
(End Galvo parameters)

(Begin Power 50  )
SC_TOOL_LOAD[1]
$SC_LASERPOWER = 50
$SC_FREQUENCY = 44.0
$SC_STDBYPERIOD = 0
$SC_JUMPSPEED = 9000
$SC_MARKSPEED = 100
$SC_JUMPDELAY = 75
$SC_MARKDELAY = 150
$SC_POLYDELAY = 0
$SC_LASERONDELAY = 70
$SC_LASEROFFDELAY = 105
SC_TOOL_SAVE[1]
(End Power 50  )

(Begin Main)
$SC_ArcStepMode = 1
$SC_DEFINES = 0
SC_TOOL[0]
G0 X0 Y0

(keep power 50 for 20 min)
$SC_LoopCount = 1200
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_ON_for_1_s
SC_End_Entity

(_________________switch OFF for 10s)
$SC_LoopCount = 10
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_OFF_for_1_s
SC_End_Entity

(switch ON for 20s)
$SC_LoopCount = 20
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_ON_for_1_s
SC_End_Entity

(_________________switch OFF for 30s)
$SC_LoopCount = 30
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_OFF_for_1_s
SC_End_Entity

(switch ON for 1 min)
$SC_LoopCount = 60
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_ON_for_1_s
SC_End_Entity

(_________________switch OFF for 2 min)
$SC_LoopCount = 120
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_OFF_for_1_s
SC_End_Entity

(switch ON for 4 min)
$SC_LoopCount = 240
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_ON_for_1_s
SC_End_Entity

(_________________switch OFF for 15min)
$SC_LoopCount = 900
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_OFF_for_1_s
SC_End_Entity

(switch ON for 15 min)
$SC_LoopCount = 900
SC_Start_Entity
SC_TOOL_LOAD[1]
CALL keep_ON_for_1_s
SC_End_Entity

SC_TOOL[0]
G0 X0 Y0
M2


DFS keep_ON_for_1_s
(1 turn lasts 0.1 s)
( 10 turns so one loop is 1 s)
SC_TOOL[1]
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
ENDDFS

DFS keep_OFF_for_1_s
(1 turn lasts 0.1 s)
( 10 turns so one loop is 1 s)
SC_TOOL[0]
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
G1 X25 Y0.0
G1 X25 Y25
G1 X0 Y25
G1 X0 Y0
ENDDFS
